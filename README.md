# Groceries

This is me following the tutorial for NativeScript's [TypeScript & Angular 2 Getting Started Guide](http://docs.nativescript.org/angular/tutorial/ng-chapter-0). The original repository lies [here](https://github.com/NativeScript/sample-Groceries/tree/angular-end).

Start-up:

tns livesync android --emulator --watch

Note to self: 
  export the android-sdk path "export ANDROID_HOME=/media/pokopikos/dev/AndroidSDKUbuntu"
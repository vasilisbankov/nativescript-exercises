import {RouterConfig} from '@angular/router';
import {nsProvideRouter} from 'nativescript-angular/router';
import {LoginPage} from './pages/login/login.component';
import {ListPage} from './pages/list/list.component';
import {ContactPage} from './pages/contact/contact.component';

export const routes: RouterConfig = [
  { path: '', component: LoginPage },
  { path: 'list', component: ListPage },
  { path: 'contacts', component: ContactPage}
];

export const APP_ROUTER_PROVIDERS = [
  nsProvideRouter(routes, {})
];
import {Component, OnInit, NgZone} from '@angular/core';
import {Contact} from '../../shared/contact/contact';
import {ContactListService} from '../../shared/contact/contact-list.service';
import "rxjs/add/operator/map";

@Component({
  selector: 'contact',
  providers: [ContactListService],
  templateUrl: 'pages/contact/contact-list.html',
  styleUrls: ['pages/contact/contact-common.css']
})

export class ContactPage implements OnInit{
  contactsList: Array<Contact> = [];

  constructor(private _contactListService: ContactListService, private _zone: NgZone){
    this.contactsList = this._contactListService.list(); // if it were an api, we would subscribe to the http events
  }
  
  ngOnInit(){

  }
  showAddress(){console.log('showAddress called')}
  toggleAddress(contact: Contact){

    this._zone.run(()=>{
      for(let i=0; i<this.contactsList.length; i++){
        this.contactsList[i].hidden = ( this.contactsList[i].name === contact.name) ? !this.contactsList[i].hidden : true;
      }
    });
    
  }

}
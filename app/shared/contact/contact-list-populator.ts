import {Contact} from './contact';

export function PopulateContacts(){
  let contacts = []

  let contactList = [
    {"name": "Shaun P. Adkins", "address": "4772 Colony Street, Hamden"},
    {"name": "Jonathan J. Nelson", "address": "3398 Whaley Lane, New Berlin"},
    {"name": "Otha R. Ferguson", "address":"3721 Hickory Lane, Washington"},
    {"name": "Richard M. Gary", "address":"3518 Front Street, Flint"},
    {"name": "Rita D. Coleman", "address":"503 Rhapsody Street, Gainesville"}
  ]
  
  for(let i=0; i<contactList.length; i++){
    let c = new Contact()
    c.name = contactList[i].name;
    c.address = contactList[i].address;
    contacts.push(c);
  }
    
  return contacts;
  
}
import {Injectable} from '@angular/core';
import {Contact} from './contact';
import {PopulateContacts} from './contact-list-populator';

@Injectable()
export class ContactListService {
  contacts: Array<Contact>;
  
  constructor(){
    // instead of calling an api, we will simulate it with a pre-populated array
    this.contacts = PopulateContacts();
  }

  list(){
    return this.contacts;
  }
  
  add(contact: Contact){
    console.log('ContactService add fired');
  }

  delete(contact: Contact){
    console.log('ContactService delete fired');
  }

}
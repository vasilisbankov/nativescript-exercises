export class Contact{
  name: string;
  address: string;
  hidden: boolean = true;
}